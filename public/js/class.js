class C {

  constructor(testNum) {
    this.testNum = testNum;
    this.dir = this._setDirArray();
    this.contrast = this._setContrastArray();
    this.currentNum = 0;
    this.currentDir = this.dir[this.currentNum];
    this.currentCont = this.contrast[this.currentNum];

    this.img = this._setupImg();
    this.cSize = 500;

    this.log = [];

    this.isEnd = false;
  }

  _setDirArray() {
    let tmp = [];

    for (let i = 0; i < this.testNum; i++) {
      tmp.push(this._randDir());
    }

    return tmp;
  }

  _setContrastArray() {
    let tmp = [];

    for (let i = 0; i < this.testNum; i++) {
      tmp.push(random([4, 3, 2]));
    }

    return tmp;
  }

  _randDir() {
    let dirs = [0, PI / 4, PI / 2, 3 * PI / 4, PI];
    let sign = [1, -1];
    let randDir = random(sign) * random(dirs);

    if(randDir === PI || randDir === -PI) {
      randDir = PI;
    }

    return randDir;
  }

  _setupImg() {
    let img = loadImage("../assert/Landolt_ring_w.png");
    imageMode(CENTER);
    return img;
  }

  _key2dir(k) {
    switch (k) {
      case 'd':
        return 0;
        break;
      case 'e':
        return -PI / 4;
        break;
      case 'w':
        return -PI / 2;
        break;
      case 'q':
        return -3 * PI / 4;
        break;
      case 'a':
        return PI;
        break;
      case 'z':
        return 3 * PI / 4;
        break;
      case 'x':
        return PI / 2;
        break;
      case 'c':
        return PI / 4;
        break;
      default:
        return -1;
    }
  }

  update() {
    if (!this.isEnd) {
      this.currentCont = this.contrast[this.currentNum];
      this.currentDir = this.dir[this.currentNum];

      translate(width / 2, height / 2);
      tint(255, this.currentCont);
      brightness(this.currentCont);
      rotate(this.currentDir);

      image(this.img, 0, 0, this.cSize, this.cSize);
    }
  }

  logger() {
    let input, correct, isCorrect;
    let contrast = this.currentCont;

    if (!this.isEnd) {

      input = this._key2dir(key);
      correct = this.currentDir;
      isCorrect = (input === correct);

      this.log.push({ input, correct, isCorrect, contrast });

      this.currentNum++;

      if (this.currentNum === this.testNum) {
        this.isEnd = true;
      }
    }
  }
}

class Noise {

  constructor(duration, db) {
    this.duration = duration;
    this.db = db;
    this.buffer = (this.db === '-inf') ? undefined : this._genBuffer();
    this.sound = new p5.SoundFile();
  }

  _db2linear(db) {
    return Math.pow(10, db / 20);
  }

  _genBuffer() {
    let bufferSize = this.duration * sampleRate();
    let channel = [];
    let buffer = new Float32Array(bufferSize);

    for (let i = 0; i < buffer.length; i++) {
      buffer[i] = randomGaussian(0, this._db2linear(this.db));
    }

    channel.push(buffer);
    channel.push(buffer);

    return channel;
  }

  play() {
    if (this.buffer) {
      this.sound.setBuffer(this.buffer);
      this.sound.setLoop(true);
      this.sound.play();
    }
  }

  resetVol(db) {
    this.stop();
    this.db = db;
    this.buffer = this._genBuffer();
    this.play();
  }

  pause() {
    this.sound.pause();
  }

  stop() {
    this.sound.stop();
  }

  isPlaying() {
    return this.sound.isPlaying();
  }
}

class ExAudio {

  constructor(numOfEx, cParam) {
    this.numOfEx = numOfEx;
    this.currentNum = 0;

    this.cs = [];
    this._setup(cParam);
    this.c = this.cs[this.currentNum];

    this.noise = new Noise(20, '-inf');
    this.vol = this.noise.db;

    this.logs = {};
    this.isEnd = false;
  }

  _setup(cParam) {
    for (let i = 0; i < this.numOfEx; i++) {
      this.cs.push(new C(cParam));
    }
  }

  update() {
    this.c.update();
  }

  logger() {
    this.c.logger();
  }

  play() {
    this.noise.play();
  }

  resetVol(db) {
    this.noise.resetVol(db);
    this.vol = this.noise.db;
  }

  stop() {
    this.noise.stop();
  }

  next() {
    if (this.c.isEnd) {

      let vol = this.vol;
      let { log } = this.c;
      this.logs[this.currentNum] = { vol, log };

      this.currentNum++;

      this.resetVol(-65 + this.currentNum * 5);

      if (this.currentNum === this.numOfEx) {
        this.isEnd = true;
      }
    }

    if (!this.isEnd) this.c = this.cs[this.currentNum];
    else {
      this.dumpLog();
      this.stop();
    }
  }

  dumpLog() {
    let postData = this.logs;
    console.log(postData);

    httpPost('http://localhost:5000/api/dumpAudio', postData, (res) => {
      console.log(res);
    });
  }
}

class ExVisual {

  constructor(voltTo, steps, cParam, socket) {
    this.steps = steps;
    this.numOfEx = voltTo / this.steps + 1;
    this.currentNum = 0;

    this.cs = [];
    this._setup(cParam);
    this.c = this.cs[this.currentNum];

    this.noise = 0;

    this.socket = socket;

    this.logs = {};
    this.isEnd = false;
  }

  _setup(cParam) {
    for (let i = 0; i < this.numOfEx; i++) {
      this.cs.push(new C(cParam));
    }
  }

  update() {
    this.c.update();
  }

  logger() {
    this.c.logger();
  }

  next() {
    if (this.c.isEnd) {

      let vol = this.noise;
      let { log } = this.c;
      this.logs[this.currentNum] = { vol, log };

      this.currentNum++;

      this.noise += this.steps;

      this.socket.emit('visualNext', { 'volt': this.noise });

      if (this.currentNum === this.numOfEx) {
        this.isEnd = true;
      }
    }

    if (!this.isEnd) this.c = this.cs[this.currentNum];
    else {
      this.dumpLog();
    }
  }

  dumpLog() {
    let postData = this.logs;
    console.log(postData);

    httpPost('http://localhost:5000/api/dumpVisual', postData, (res) => {
      console.log(res);
    });
  }
}
