/**
* T E S T 1
*
* High Contrast is an awesome Drum & Bass Producer / DJ.
* If We Ever, Kiss Kiss Bang Bang, Twilight's Last Gleaming etc...
*
*/

const cSize = 200;
const N = 100;

let img;
let contrast = 255;
let count = 0;
let dir = [];
let typedDir = 0;
let typedLog = [];
let timeLog = [];

function setup() {

  createCanvas(720, 400);
  img = loadImage("assert/Landolt_ring_0.png");
  imageMode(CENTER);

  frameRate(1);

  for(let i = 0; i < N; i++) {
    dir[i] = randDir();
  }
}

function draw() {

  background(255);

  drawC(contrast, dir[count]);

  console.log("sen:", contSen(contrast));
  console.log("cont", contrast);

  if(contrast === 1) {

    noLoop();
    console.log('it is over.');

    for(let i = 0; i < typedLog.length; i++) {

      let key = typedLog[i];
      let isMatch;

      (key2dir(key) === dir[i]) ? isMatch = true : isMatch = false;

      console.log(dir[i] + ':' + key2dir(key) + '=>' + isMatch);
    }
  }
}

function keyTyped() {

  if(!isNaN(Number(key)))
    typedLog.push(Number(key));
  else
    typedLog.push(w2n(key));

  contrast = round(contrast * exp(-count/50.0));
  count++;
}
