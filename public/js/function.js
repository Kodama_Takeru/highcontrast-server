/***
*
* Draw 'C' cymbol.
*
* @param {Number} contrast
* @param {Number} direction
*
*/
function drawC(contrast, direction) {

  translate(width/2, height/2);
  tint(255, contrast);
  brightness(contrast);
  rotate(direction);

  image(img, 0, 0, cSize, cSize);
}

/***
*
* Generate random direction between the 0 to pos/neg PI.
*
* @return {Number}
*
*/
function randDir() {

  let dirs = [0, PI / 4, PI / 2, 3 * PI / 4, PI];
  let sign = [1, -1];

  let randDir = random(sign) * random(dirs);

  if(randDir === PI || randDir === -PI) {
    randDir = PI;
  }

  return randDir;
}

/***
*
* contSen
*
* @param {Number} contrast
* @return {Number}
*
*/
function contSen(contrast) {

  let cs = (255 + contrast) / (255 - contrast);

  return cs;
}

/**
*
* key2dir
*
* @param key number
*
*/

function key2dir(key) {

  switch(key) {
    case 6:
      return 0;
      break;
    case 9:
      return -PI / 4;
      break;
    case 8:
      return -PI / 2;
      break;
    case 7:
      return -3 * PI / 4;
      break;
    case 4:
      return PI;
      break;
    case 1:
      return 3 * PI / 4;
      break;
    case 2:
      return PI / 2;
      break;
    case 3:
      return PI / 4;
      break;
    default:
      return -1;
  }
}

/**
*
* w2n
*
* @param w string
* @return number
*
*/

function w2n(w) {

  switch(w) {
    case 'd':
      return 6;
      break;
    case 'e':
      return 9;
      break;
    case 'w':
      return 8;
      break;
    case 'q':
      return 7;
      break;
    case 'a':
      return 4;
      break;
    case 'z':
      return 1;
      break;
    case 'x':
      return 2;
      break;
    case 'c':
      return 3;
      break;
    default:
      return -1;
  }
}

/**
*
* timer
*
* @param dateo object
* @return number
*
*/

function timer(dateo) {

  let daten = new Data();
  let milisecn = date.getTime();
  let miliseco = daten.getTime();

  return miliseco - milisecn;
}

/**
*
* arrAve
*
* @param array array
* @return number
*/

function arrAve(array) {

  return array.reduce((prev, current, i, array) => {
    return prev + current;
  }) / array.length;
}

/**
*
* ssd: sample standard deviation
*
* @param {Array} array
* @return {Number}
*/
function ssd(array) {

  let ave = arrAve(array);
  let tmp = 0;

  for(let a of array) {
    tmp += (a - ave) ** 2;
  }

  return Math.sqrt(tmp / (array.length));
}

/**
*
* dump result
*
* TODO: WIP
*
* @param {Array} array
*/
function dumpResult(array) {

  let table = new p5.Table;

  table.addColumn('contrast');
  table.addColumn('answer');
  table.addColumn('time');
}

function generateWGNBuffer(sd, sec) {

  let bufferSize = sec * sampleRate();
  let channel = [];
  let buffer = new Float32Array(bufferSize);

  for (let i = 0; i < buffer.length; i++) {
    buffer[i] = randomGaussian(0, sd);
  }

  channel.push(buffer);
  channel.push(buffer);

  console.log(channel.length)

  return channel;
}

function db2linear(db) {
  return Math.pow(10, db / 20);
}
