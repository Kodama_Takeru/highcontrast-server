/**
* T E S T 2
*
* High Contrast is an awesome Drum & Bass Producer / DJ.
* If We Ever, Kiss Kiss Bang Bang, Twilight's Last Gleaming etc...
*
*/

const cSize = 200;
const N = 25;

let img;
let contrast = 3;
let count = 0;
let dir = [];
let typedDir = 0;
let typedLog = [];
let timeLog = [];
let contLog = [];
let timeResFeb = [];
let timeResMar = [];

const time = new Date();
let initTime;

function setup() {

  createCanvas(720, 400);
  img = loadImage("assert/Landolt_ring_0.png");
  imageMode(CENTER);

  frameRate(15);

  for(let i = 0; i < N; i++) {
    dir.push(randDir());
  }

  initTime = time.getTime();
}

function draw() {

  background(255);

  drawC(contrast, dir[count]);

  if(count > N - 1) {

    noLoop();
    console.log('it is over.');

    let key;
    let feb = 0;
    let febCount = 0;
    let mar = 0;
    let marCount = 0;
    let isMatch;

    for(let i = 0; i < typedLog.length; i++) {

      key = typedLog[i];

      if(contLog[i] === 2) {
        if(key2dir(key) === dir[i]) feb++;
        febCount++;

        if(i == 0) {
          timeResFeb.push(timeLog[0] - initTime);
        } else {
          timeResFeb.push(timeLog[i] - timeLog[i - 1]);
        }

      } else {
        if(key2dir(key) === dir[i]) mar++;
        marCount++;

        if(i == 0) {
          timeResMar.push(timeLog[0] - initTime);
        } else {
          timeResMar.push(timeLog[i] - timeLog[i - 1]);
        }
      }
    }

    console.log('cont2: ', feb/febCount * 100, '% | time ave:', arrAve(timeResFeb));
    console.log('cont3: ', mar/marCount * 100, '% | time ave:', arrAve(timeResMar));
  }
}

function keyTyped() {

  const d = new Date();

  if(!isNaN(Number(key)))
    typedLog.push(Number(key));
  else
    typedLog.push(w2n(key));

  contLog.push(contrast);
  timeLog.push(d.getTime());

  contrast = random([3, 2]);
  count++;
}
