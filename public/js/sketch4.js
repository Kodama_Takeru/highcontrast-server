/**
* T E S T 3
*
* High Contrast is an awesome Drum & Bass Producer / DJ.
* If We Ever, Kiss Kiss Bang Bang, Twilight's Last Gleaming etc...
*
*/

const cSize = 200;
const N = 50;

let img;
let contrast = 3;
let count = 0;
let dir = [];
let typedDir = 0;
let typedLog = [];
let timeL = [];
let contLog = [];
let timeResFeb = [];
let timeResMar = [];

const contFrom = 2;
const contTo = 4;

const time = new Date();
let initTime;

function setup() {

  createCanvas(400, 400);
  img = loadImage("assert/Landolt_ring_w.png");
  imageMode(CENTER);

  for(let i = 0; i < N; i++) {
    dir.push(randDir());
  }

  initTime = time.getTime();
}

function draw() {

  background(256 / 2 + randomGaussian(0, 10));
  // for (let j = 0; j < height; j++) {
  //   for (let i = 0; i < width; i++) {
  //     let n = random([(256 / 2) - 1, (256 / 2) + 1]);
  //     noStroke();
  //     fill(n);
  //     rect(i*1, j*1, 1, 1);
  //   }
  // }

  drawC(contrast, dir[count]);

  if(count > N - 1) {

    noLoop();
    console.log('it is over.');

    let key;
    let count = [];
    let timeLog = [];

    for(let j = contFrom; j <= contTo; j++) {

      let timeLogTmp = [];
      let tmpC = [];

      for(let i = 0; i < typedLog.length; i++) {

        key = typedLog[i];

        if(contLog[i] === j) {

          if(key2dir(key) === dir[i]) {
            tmpC.push(1);
          }
          else {
            tmpC.push(0);
          }

          if(i === 0) {
            timeLogTmp.push(timeL[0] - initTime);
          }
          else {
            timeLogTmp.push(timeL[i] - timeL[i - 1]);
          }
        }
      }

      count.push(tmpC);
      timeLog.push(timeLogTmp);
    }

    console.log('contrast, answer ave, sample standard deviation, time ave, sample standard deviation');
    for(let i = 0; i <= contTo - contFrom; i++) {
      console.log(i + contFrom, arrAve(count[i]), ssd(count[i]), arrAve(timeLog[i]), ssd(timeLog[i]));
    }
  }
}

function keyTyped() {

  const d = new Date();

  if(!isNaN(Number(key)))
    typedLog.push(Number(key));
  else
    typedLog.push(w2n(key));

  contLog.push(contrast);
  timeL.push(d.getTime());

  contrast = random([4, 3, 2]);
  count++;
}
