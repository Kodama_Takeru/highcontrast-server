/**
* T E S T 5
*
* High Contrast is an awesome Drum & Bass Producer / DJ.
* If We Ever, Kiss Kiss Bang Bang, Twilight's Last Gleaming etc...
*
*/

const socket = io();
let noise;
let img;

let backgroundCont = 120;
let cCont = 10;

function setup() {
  noise = new Noise(20, -5);

  createCanvas(windowWidth, windowHeight);
  img = loadImage("../assert/Landolt_ring_w.png");
  imageMode(CENTER);
}

function draw() {
  background(backgroundCont);
  translate(width / 2, height / 2);
  tint(255, cCont);
  brightness(cCont);

  image(img, 0, 0, height / 1.5, height / 1.5);
}

socket.on('emit', () => {
  if (noise.isPlaying()) {
    noise.pause();
  }
  else {
    noise.play();
  }
});
