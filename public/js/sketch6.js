// High Contrast - Make It Tonight

let ex;

function setup() {
  ex = new ExAudio(9, 50);
  ex.update();

  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(256 / 2);
  ex.update();
}

function keyTyped() {
  if (!ex.isEnd) {
    ex.logger();

    if (ex.c.isEnd) {
      ex.next();
    }
  }
}
