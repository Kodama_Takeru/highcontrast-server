// High Contrast - Make It Tonight

let ex;
const socket = io();

function setup() {
  ex = new ExVisual(2.5, 0.25, 10, socket);
  ex.update();

  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(256 / 2);
  ex.update();
}

function keyTyped() {
  if (!ex.isEnd) {
    ex.logger();

    if (ex.c.isEnd) {
      ex.next();
    }
  }
}
