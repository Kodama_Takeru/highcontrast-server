const express = require('express');
const app = express();
const http = require('http').Server(app);
const bodyParser = require('body-parser');
const io = require('socket.io')(http);

const port = process.env.PORT || 5000;

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

const api = require('./routes/api');
app.use('/api', api);

const board = process.env.BOARD || '/dev/cu.usbmodem141301';

const SerialPort = require('serialport')
const serial = new SerialPort(board, {
  bandRate: 9600
});

http.listen(port, function() {
  console.log("Node app is running at localhost:" + port);
});

serial.on('open', () => {
  console.log('Port is open!');
  setInterval(write, 1000, 'O');  // Communicate with Arduino at all times to keep connection with that.
});

serial.on('close', () => {
  console.log('Serial port disconnected.');
});

io.on('connection', (socket) => {
  serial.on('data', function(data) {
    console.log('data: ' + data);
    socket.emit('emit');
  });

  socket.on('visualNext', ({ volt }) => {
    serial.write(new Buffer('K'), function(err, results) {
      if(err) {
        console.log('Err: ' + err);
        console.log('Results: ' + results);
      }
    });
  })
});

function write(data) {
  serial.write(new Buffer(data), function(err, results) {
    if(err) {
      console.log('Err: ' + err);
      console.log('Results: ' + results);
    }
  });
}
