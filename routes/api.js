const express = require('express');
const router = express.Router();
const fs = require('fs');

router.post('/dumpAudio', (req, res) => {

  let body = req.body;

  let csv = makeCsv(body);

  if (!fs.existsSync('./results')) fs.mkdirSync('./results');
  let date = new Date();
  fs.writeFile(`./results/audio_${date.toString()}.csv`, csv, () => {
    console.log('csv dumped.');
  });

  res.json({ 'msg': 'done' });
});

router.post('/dumpVisual', (req, res) => {

  let body = req.body;

  let csv = makeCsv(body);

  if (!fs.existsSync('./results')) fs.mkdirSync('./results');
  let date = new Date();
  fs.writeFile(`./results/visual_${date.toString()}.csv`, csv, () => {
    console.log('csv dumped.');
  });

  res.json({ 'msg': 'done' });
});

function modres({ vol, log }) {
  let cont = [];

  log.forEach(d => {
    cont.push(d.contrast);
  });

  let contrast = cont.filter((c, i, self) => {
    return self.indexOf(c) === i;
  });
  contrast.sort();

  let res = {};

  contrast.forEach(c => {
    let thisCont = log.filter((d) => {
      return d.contrast === c;
    });
    let correct = thisCont.filter((t) => {
      return t.isCorrect === true;
    });
    res[c] = { 'num': thisCont.length, 'correct': correct.length };
  });

  return { vol, res };
}

function makeCsv(body) {
  let csv = [];

  csv.push(',Vol');
  Object.keys(modres(body[0]).res).forEach(k => {
    csv.push(k);
    csv.push('ssd');
  });
  csv.push('\n');

  for (let key of Object.keys(body)) {
    let { vol, res } = modres(body[key]);
    csv.push(vol);

    Object.keys(res).forEach(k => {
      csv.push(res[k].correct / res[k].num);
      csv.push(ssd(res[k]));
    });

    csv.push('\n');
  }

  return csv.join();
}

function ssd({ num, correct }) {
  let tmp = 0;
  let ave = correct / num;
  let incorrect = num - correct;

  for(let i = 0; i < correct; i++) {
    tmp += (1 - ave) ** 2;
  }
  for(let i = 0; i < incorrect; i++) {
    tmp += (0 - ave) ** 2;
  }

  return Math.sqrt(tmp / (num));
}

module.exports = router;
