#include <Gaussian.h>

int incomingByte = 0;
int count = 0;
float STEP = 0.25;
float d = 0.0;

void setup() {
  Serial.begin(9600);
  pinMode(3, OUTPUT);
  randomSeed(analogRead(0));
}

void loop() {
  Gaussian g = Gaussian(255 / 2, d);
  analogWrite(3, g.random());
  
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    if (incomingByte != 79) {
      count++;
      d = mapFloat(count * STEP, 0, 5.0, 0, 255);
    }
  }
  delay(10);
}

float mapFloat(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
