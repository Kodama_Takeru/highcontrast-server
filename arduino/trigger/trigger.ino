const byte TRIG = 2;
int count = -1;

void setup() {
  pinMode(TRIG, INPUT_PULLUP);
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(TRIG), counter, RISING);

  pinMode(13, OUTPUT);
}

void loop() {
  digitalWrite(13, HIGH);
  delay(1);
  digitalWrite(13, LOW);
  delay(2000);
}

void counter() {
  count++;
  if (count == 10) {
    emit();
    count = 0;
  }
}

void emit() {
  Serial.println(1);
}
